package com.example.lecture7

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.view.View
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private val images = arrayOf(R.mipmap.heic1509a,R.mipmap.ic_launcher,R.mipmap.ic_launcher_round,R.mipmap.goat)
    private val ImgViews= mutableListOf<ImageView>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init ()
    }

    private fun init()
    {
        ImgViews.add(img1)
        ImgViews.add(img2)
        ImgViews.add(img3)
        ImgViews.add(img4)
        img1.setOnClickListener(this)
        img2.setOnClickListener(this)
        img3.setOnClickListener(this)
        img4.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        ImgViews[(0 until ImgViews.size).random()].setImageResource(images[(images.indices).random()])
    }

}
